// All display related functions

void showGreeting(){
  lcd.print("Arduinometer ");
  lcd.write((byte)0x0);
  lcd.setCursor(0,1);
  lcd.print("Firmware v 1.2");
  delay(3000);
  lcd.createChar(0, none);
  showTimers(activeMode);
}

void showMode(int globalMode){
  lcd.clear();
  lcd.noBlink();
  lcd.noCursor();
  lcd.setCursor(0,0);
  lcd.print("------Mode------");
  lcd.setCursor(0,1);
  switch (globalMode) {
  case 0:
    lcd.print("Bulb");
    break;
  case 1:
    lcd.print("Bulb + TimeLapse");
    break;
  case 2:
    lcd.print("Bulb + Trigger");
    break;
  case 3:
    lcd.print("TimeLapse");
    break;
  case 4:
    lcd.print("Trigger");
    break;
  }
}

void showTimers(int modeToDisplay){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.cursor();
  //lcd.blink();

  if (modeToDisplay == 0 || modeToDisplay == 1 || modeToDisplay == 2){ // sets display up for bulb modes
    lcd.print("Bulb");
    lcd.setCursor(8,0);
    lcd.print(convertTime(bulbTime));
  }
  else if (modeToDisplay == 3 || modeToDisplay == 4){
    lcd.print("Camera setting");                                     // bulb not active
  }
  lcd.setCursor(0,1);
  if (modeToDisplay == 1 || modeToDisplay == 3){                     // sets display up for timelapse modes
    lcd.print("TLapse");
    lcd.setCursor(8,1);
    lcd.print(convertTime(timelapseTime));
  }
  else if (modeToDisplay == 2 || modeToDisplay == 4){                // sets up display for trigger modes
    lcd.print("TR"); 
    triggerSensDisplay();
  }
  lcd.setCursor(7,inputLineSelect);
}

void triggerSensDisplay(){
  //lcd.noBlink();
  lcd.setCursor(2,1);
  lcd.print(fourDigits(levelSetting));
  glyphSet = levelSetting / 146;
  lcd.write(glyphSet);
  lcd.setCursor(2,1);
}

void yellow(){
  analogWrite(ledGreen, 120);
  analogWrite(ledRed, 130);
}

void green(){
  analogWrite(ledGreen, 255);
  analogWrite(ledRed, 0);
}

void red(){
  analogWrite(ledGreen, 0);
  analogWrite(ledRed, 255);
}
