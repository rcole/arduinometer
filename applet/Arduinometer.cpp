/*
Camera intervalometer and Time Lapse controller
 
 The circuit:
 LCD pins
 * LCD (4) RS pin to digital pin 13
 * LCD (6) Enable pin to digital pin 12
 * LCD (11) D4 pin to digital pin 11
 * LCD (12) D5 pin to digital pin 10
 * LCD (13) D6 pin to digital pin 9
 * LCD (14) D7 pin to digital pin 8
 * LCD (5) R/W pin to ground
 * 10K pot:
 *   ends to +5V and ground
 *   wiper to LCD (3) VO pin (pin 3) for contrast
 *   Vss (1) to ground
 *   Vdd (2) to +5v
 
 Button and encoder pins
 * Mode selection button to A0
 * Input selection button to A1
 * start button pin A2
 * encoder pins D2 & D4
 * indicator light D5 & D6 PWM
 
 To add
 * relay controls + pins and wiring
 
 -relay logic
 
 by Ryan Cole - 2011 
 Open source and shit
 */


// PIN VARIABLES
#include "WProgram.h"
void setup();
void loop();
void bulbMode();
void bulbTimeLapseMode();
void bulbTrigMode();
void timeLapseMode();
void trigMode();
void playButton();
void inputSelectorButton();
void modeSelectorButton();
void resetCounters();
void showGreeting();
void showMode(int globalMode);
void showTimers(int modeToDisplay);
void triggerSensDisplay();
void yellow();
void green();
void red();
void doEncoder();
char* convertTime(int time);
char* fourDigits(int level);
void bulbCounter();
void timelapseCounter();
void bulbWhile();
void timelapseWhile();
void triggerWhile();
void readSensor();
void openShutter();
void closeShutter();
const int runSwitch = A0;     // pin for run switch
const int inputSelector = A1; // pin for input_selector button
const int modeSelector = A2;  // pin for modeSelector button

const int encoder0PinA = 2;   // encoder A pin
const int encoder0PinB = 4;   // encoder B pin

const int ledRed = 5;         // pins for indicator LED
const int ledGreen = 6;       

const int triggerInput = A3;  // pin for trigger sensor in


// LOGIC VARIABLES

int bulbTime = 0;            // this is the value to be incremented for bulb timer
int timelapseTime = 0;       // this is the value to be incremented for time lapse timer

const int numRows = 2;       // number of rows and cols in LCD
const int numCols = 16;

int activeMode = 0;          // stores the operation mode. Should loop from 0-4. See the 'switch' section below for what each number is.
int inputLineSelect = 0;     // switches between 1 and 0 every time the inputSelector button is pressed, and selects the LCD row.
int playPause = 0;           // button state for run button. 0 when paused, 1 when running

int inputBtnPressed = 1;     // inputBtnPressed = 0 when the button is up, and 1 when the button is pressed.
int modeBtnPressed = 1;      // modeBtnPressed = 0 when the button is not pressed, and 1 when the button is pressed
int runBtnPressed = 0;       // button state for play btn.

long lastTick = 0;           // stores a time from millis() to denote passage of time. See timer functions.
int timelapseTimeI;          // stores original value of TL to reset after countdown.
int bulbTimeI;               // does the same for BT
int levelSetting = 0;        // trigger threshold set by user
int sensorLevel = 0;         // the actual value read off the sensor pin

char* hh;                    // variables for timer function
char* mm;
char* ss;
int hhI;
int mmI;
int ssI;
static char buffer[9];           // for time count conversion. holds 9 digits to account for 00:00:00

static char trigSetBuffer[5];    // holds converted 4 digit display of levelSetting
static char trigInputbuffer[5];  // holds converted 4 digit display of sensorLevel
int glyphSet;                    // holds bar graph glyph for visual feedback on levelSetting
int glyphIn;                     // holds bar graph glyph for visual feedback on sensorLevel. I added this as a visual indicator of input levels.

byte smiley[8] = {               // byte data for custom glyphs in bar graph. one for each of 7 levels
  B00000,
  B10001,
  B00000,
  B00000,
  B10001,
  B01110,
  B00000,
};

byte trig[8] = {
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
};

byte none[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
};

byte one[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B11111,
};

byte two[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B11111,
  B11111,
};

byte three[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B11111,
  B11111,
  B11111,
};

byte four[8] = {
  B00000,
  B00000,
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
};

byte five[8] = {
  B00000,
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
};
byte six[8] = {
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
};
byte seven[8] = {
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
};

#include <stdio.h>                        // include the string voodoo library for converting timers to hh:mm:ss
#include <LiquidCrystal.h>                // include the lcd library and initialize which pins are in use
LiquidCrystal lcd(13, 12, 11, 10, 9, 8);  


void setup() {
  //Serial.begin(9600);                     // Initialize the Serial port at 9600 baud. For debugging only
  pinMode(inputSelector,INPUT);           // inputLineSelect button input
  digitalWrite(inputSelector, HIGH);      // turn on pullup resistor
  pinMode(modeSelector, INPUT);           // mode selector pin setup
  digitalWrite(modeSelector, HIGH);       // turn on pullup resistor
  pinMode(runSwitch, INPUT);              // mode selector pin setup
  digitalWrite(runSwitch, HIGH);          // turn on pullup resistor

  pinMode(encoder0PinA, INPUT);           // encoder pin A setup
  digitalWrite(encoder0PinA, HIGH);       // turn on pullup resistor
  pinMode(encoder0PinB, INPUT);           // encoder pin b setup
  digitalWrite(encoder0PinB, HIGH);       // turn on pullup resistor

  pinMode(ledRed, OUTPUT);                // red led
  pinMode(ledGreen, OUTPUT);              // green led

  pinMode(triggerInput, INPUT);           // trigger input

  analogWrite(ledRed, 255);               // turn on red LED for power!
  lcd.createChar(0, smiley);              // converts byte data into usable glyphs. 8 max, so smiley gets killed for 'none' later on.
  lcd.createChar(1, one);
  lcd.createChar(2, two);
  lcd.createChar(3, three);
  lcd.createChar(4, four);
  lcd.createChar(5, five);
  lcd.createChar(6, six);
  lcd.createChar(7, seven);
  lcd.begin(numCols,numRows);             // set up the LCD's number of columns and rows:
  showGreeting();                         // greeting screen on start up. Comment this out if it's annoying.
  //showTimers(activeMode);               // uncomment this if you remove showGreeting(); as no display function will be called otherwise
}

void loop() {
  playButton();                                                        // make the LED red when not running.
  if (playPause == 0){
    analogWrite(ledRed, 255);
    analogWrite(ledGreen, 0);
  } 

  switch (playPause) {
  case 0:                                                              // Run switch is off!!
    attachInterrupt(0, doEncoder, CHANGE);                             // encoder on interrupt 0  
    inputSelectorButton();                                             // listen to input button
    modeSelectorButton();                                              // listen to mode button 
    resetCounters();                                                   // listen to counter reset
    if (activeMode == 0){inputLineSelect = 0;}                         // make sure that the cursor falls on the right line for the selected mode.
    if (activeMode == 3 || activeMode == 4){inputLineSelect = 1;}
    break;
  case 1:                                  // Run switch is on!
    detachInterrupt(0);                    // disable encoder so we don't change a value in use.
    switch (activeMode) {                  // detect what mode is operating and chose the right operations
    case 0:                                // run bulb mode
      bulbMode();
      break;
    case 1:                                // run bulb + timelapse mode
      bulbTimeLapseMode();
      break;
    case 2:                                // run bulb + trigger mode
      bulbTrigMode();
      break;
    case 3:                                // run time lapse mode
      timeLapseMode();
      break;
    case 4:                                // run trigger mode
      trigMode();
      break;
    }
  }
}

void bulbMode(){
  bulbTimeI = bulbTime;                      // store orignal value for future use
  openShutter();                             // open camera shutter
  bulbWhile();                               // count off bulb time
  closeShutter();                            // close camera shutter
  bulbTime = bulbTimeI;                      // reset BT with original value incase you want to run the same exposure again. 
  lcd.setCursor(8,0);                        
  lcd.print(convertTime(bulbTime));          // print it to the display
  playPause = 0;                             // end run
}                                            // this only runs once

void bulbTimeLapseMode(){
  timelapseTimeI = timelapseTime;
  bulbTimeI = bulbTime;
  timelapseWhile();                          // count down TL time and progress when 0
  timelapseTime = timelapseTimeI;            // reset TL to original time value
  lcd.setCursor(8,1);
  lcd.print(convertTime(timelapseTime));     // print it to the display
  if (playPause == 1) openShutter();         // open camera shutter
  bulbWhile();                               // count off bulb time value
  closeShutter();                            // close camera shutter
  bulbTime = bulbTimeI;                      // reset BT with original value
  lcd.setCursor(8,0);
  lcd.print(convertTime(bulbTime));          // print it to the display.
}                                            // rinse, and repeat. This will run till you stop it.

void bulbTrigMode(){
  bulbTimeI = bulbTime;
  triggerWhile();                            // listen to sensor and progress when threshold is met.
  if (playPause == 1) openShutter();         // open camera shutter.
  bulbWhile();                               // count down bulb time and progress when 0
  closeShutter();                            // close camera shutter
  bulbTime = bulbTimeI;                      // reset LCD with original value
  lcd.setCursor(8,0);
  lcd.print(convertTime(bulbTime));          
  playPause = 0;                             // end run after time elapses
}

void timeLapseMode(){
  timelapseTimeI = timelapseTime;            // store orignal value for future use
  timelapseWhile();                          // count down TL time and progress when 0
  timelapseTime = timelapseTimeI;            // reset Tl to original time value
  lcd.setCursor(8,1);
  lcd.print(convertTime(timelapseTime));     // reset LCD with original value
  if (playPause == 1) openShutter();         // open camera shutter
  closeShutter();                            // close camera shutter. The actual exposure time should be set on the camera.
  }                                          // rinse, and repeat. This will run till you stop it.


void trigMode(){
  while (playPause == 1){
  triggerWhile();                            // listen to sensor and progress when threshold is met.             
  if (playPause == 1) openShutter();         // open camera shutter.
  closeShutter();                            // close camera shutter. The actual exposure time should be set on the camera.
  playPause = 0;                             // end run.
  }
}

void playButton(){
  if ((digitalRead(runSwitch) == LOW) && (runBtnPressed == 0)) // run selector Button is pressed. 
  {
    runBtnPressed = 1;                           
    //Serial.println("play pressed");
    if (playPause == 1){
      playPause = 0;
      closeShutter();
    }
    else {
      playPause=1;
    }
  }
  else if ((digitalRead(runSwitch) == HIGH) && (runBtnPressed == 1)) // run Button is released. 
  {
    //Serial.println("play released");
    runBtnPressed = 0;                     // Avoids held press repeating.
    delay(100);                            // button bounce.
  }
}

void inputSelectorButton(){
  if (activeMode == 1 || activeMode == 2){                             // only respond if we're in a mode with 2 values to edit 
    if ((digitalRead(inputSelector) == LOW) && (inputBtnPressed == 0)) // input selector Button is pressed, but was previously not 
    {                                                                  // This switches the line the cursor is set on
      inputBtnPressed = 1;     
      if (inputLineSelect==1){
        inputLineSelect=0;
        lcd.setCursor(7,0);
      }
      else {
        inputLineSelect=1;
        lcd.setCursor(7,1);
      }
    }
    else if ((digitalRead(inputSelector) == HIGH) && (inputBtnPressed == 1)) // input selector Button is released, but was previously was pressed
    {
      inputBtnPressed = 0;                                                   // Avoids held press repeating.
      delay(100);                                                            // small delay to account for button bounce.
    }
  }
}

void modeSelectorButton(){
  if ((digitalRead(modeSelector) == LOW) && (modeBtnPressed == 0)) // mode selector Button is pressed. 
  {
    modeBtnPressed = 1;                // mode selector Button press is registered
    if (activeMode < 4){               // Here is control logic for incrementing the mode variable
      activeMode++;
    }
    else{
      activeMode = 0;
    }
    showMode(activeMode);
  }
  else if ((digitalRead(modeSelector) == HIGH) && (modeBtnPressed == 1)) // mode selector Button is released. 
  {
    modeBtnPressed = 0;                 // Avoids held press repeating.
    delay(300);                         // small delay to account for button bounce.
    showTimers(activeMode);
  }
}

void resetCounters(){
  if ((digitalRead(modeSelector) == LOW) && (digitalRead(runSwitch) == LOW)) // mode selector Button is pressed.
  { 
    lcd.clear();
    lcd.print("reset counters");
    showTimers(activeMode);
  }
}



// All display related functions

void showGreeting(){
  lcd.print("Arduinometer ");
  lcd.write(0);
  lcd.setCursor(0,1);
  lcd.print("Firmware v 1.0");
  delay(3000);
  lcd.createChar(0, none);
  showTimers(activeMode);
}

void showMode(int globalMode){
  lcd.clear();
  lcd.noBlink();
  lcd.noCursor();
  lcd.setCursor(0,0);
  lcd.print("------Mode------");
  lcd.setCursor(0,1);
  switch (globalMode) {
  case 0:
    lcd.print("Bulb");
    break;
  case 1:
    lcd.print("Bulb + TimeLapse");
    break;
  case 2:
    lcd.print("Bulb + Trigger");
    break;
  case 3:
    lcd.print("TimeLapse");
    break;
  case 4:
    lcd.print("Trigger");
    break;
  }
}

void showTimers(int modeToDisplay){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.cursor();
  //lcd.blink();

  if (modeToDisplay == 0 || modeToDisplay == 1 || modeToDisplay == 2){ // sets display up for bulb modes
    lcd.print("Bulb");
    lcd.setCursor(8,0);
    lcd.print(convertTime(bulbTime));
  }
  else if (modeToDisplay == 3 || modeToDisplay == 4){
    lcd.print("Camera setting");                                     // bulb not active
  }
  lcd.setCursor(0,1);
  if (modeToDisplay == 1 || modeToDisplay == 3){                     // sets display up for timelapse modes
    lcd.print("TLapse");
    lcd.setCursor(8,1);
    lcd.print(convertTime(timelapseTime));
  }
  else if (modeToDisplay == 2 || modeToDisplay == 4){                // sets up display for trigger modes
    lcd.print("TR"); 
    triggerSensDisplay();
  }
  lcd.setCursor(7,inputLineSelect);
}

void triggerSensDisplay(){
  //lcd.noBlink();
  lcd.setCursor(2,1);
  lcd.print(fourDigits(levelSetting));
  glyphSet = levelSetting / 146;
  lcd.write(glyphSet);
  lcd.setCursor(2,1);
}

void yellow(){
  analogWrite(ledGreen, 120);
  analogWrite(ledRed, 130);
}

void green(){
  analogWrite(ledGreen, 255);
  analogWrite(ledRed, 0);
}

void red(){
  analogWrite(ledGreen, 0);
  analogWrite(ledRed, 255);
}
void doEncoder() {
  /* If pinA and pinB are both high or both low, it is spinning
   * forward. If they're different, it's going backward.
   */
  lcd.setCursor(8,inputLineSelect);
  // encoder is moving clockwise
  if (digitalRead(encoder0PinA) != digitalRead(encoder0PinB)) {                                    
    if ((inputLineSelect == 0) && ((activeMode == 0) || (activeMode == 1) || (activeMode == 2))){  // input line is 0 and a bulb mode is selected
      if (bulbTime > 240){ //increase increment
        bulbTime += 10;
      }
      else {
        bulbTime++; // increase by 1
      }
      lcd.print(convertTime(bulbTime));  
    }

    else if ((inputLineSelect == 1) && ((activeMode == 1) || (activeMode == 3))){
      if (timelapseTime > 350){  // increase increment when the value is higher
        timelapseTime += 10;
      }
      else {
        timelapseTime++;
      }
      lcd.print(convertTime(timelapseTime));
    }
    else if ((inputLineSelect == 1) && ((activeMode == 2) || (activeMode == 4))){
      if (levelSetting < 1024){  // don't increase past 1024
         levelSetting += 5;
         triggerSensDisplay();
      }
    }
  } 
  // encoder is moving counter clockwise
  else if (digitalRead(encoder0PinA) == digitalRead(encoder0PinB)) {                                     
    if ((inputLineSelect == 0) && ((activeMode == 0) || (activeMode == 1) || (activeMode == 2)) && (bulbTime > 0)){ // input line is 0 and a bulb mode is selected
      if (bulbTime > 250){ // increase decrement when the value is higher
        bulbTime -= 10;
      }
      else {
        bulbTime--; // decrement by 1
      }
      lcd.print(convertTime(bulbTime));
    }
    else if ((inputLineSelect == 1) && ((activeMode == 1) || (activeMode == 3)) && (timelapseTime > 0)){
      if (timelapseTime > 360){
        timelapseTime -= 10;
      }
      else {
        timelapseTime--;
      }
      lcd.print(convertTime(timelapseTime));
    }
    else if ((inputLineSelect == 1) && ((activeMode == 2) || (activeMode == 4))){
      if (levelSetting > 0){
         levelSetting -= 5;
         triggerSensDisplay();      // since this is a bit more fancy it's done in a seperate function.
      }
    }
  }
  if ((inputLineSelect != 1) && ((activeMode != 2) || (activeMode != 4))){
        lcd.setCursor(7,inputLineSelect);
  }
}

// Not exactly all time related, but most of the functional guts are in here.

char* convertTime(int time) {
  hhI=time/3600;
  mmI=(time-hhI*3600)/60;
  ssI=time-(hhI*3600)-(mmI*60);
  sprintf(buffer, "%02d:%02d:%02d", hhI, mmI, ssI);
  return buffer;
}

char* fourDigits(int level){
  sprintf(trigSetBuffer, "%04d", level);
  return trigSetBuffer;
}

void bulbCounter(){
  if (millis() - lastTick >= 1000) {
    lastTick = millis();
    bulbTime--;
    lcd.setCursor(8,0);
    lcd.print(convertTime(bulbTime)); 
    //Serial.println(bulbTime);
  } 
}

void timelapseCounter(){
  if (millis() - lastTick >= 1000) {
    lastTick = millis();
    timelapseTime--;
    lcd.setCursor(8,1);
    lcd.print(convertTime(timelapseTime)); 
    //Serial.println(timeLapseTime);
  } 
}

void bulbWhile(){
  while(bulbTime > 0){                       // countdown bulb value
    bulbCounter();
    playButton();
    if (playPause == 0){
      break;
    }
  }
}

void timelapseWhile(){
  while(timelapseTime > 0){                  // count down TL value
    timelapseCounter();
    yellow();                                 
    playButton();
    if (playPause == 0){
      break;
    }  
  }
}

void triggerWhile(){
  while (sensorLevel <= levelSetting){
    yellow();                                // make LED yellow to indicate TL running. 
    playButton();
    readSensor();
    if (playPause == 0){
      break;
    }
  }
}

void readSensor(){                              // obviously to read the sensor :)
  if (millis() - lastTick >= 50) {
    lastTick = millis();
    sensorLevel = analogRead(triggerInput);
    lcd.setCursor(9,1);
    lcd.print(fourDigits(sensorLevel));
    glyphSet = sensorLevel / 146;
    lcd.write(glyphSet);
    lcd.setCursor(9,1);
  } 
}

void openShutter(){                            // open the camera shutter
  //Serial.println("shutter opened");
  green();
  delay(200);                                  // the delay here is meant to allow the camera time to respond. Might need to tweak the value.
}

void closeShutter(){
  //Serial.println("shutter closed");          // close the camera shutter
  red();
}



int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

