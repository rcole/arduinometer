void doEncoder() {
  /* If pinA and pinB are both high or both low, it is spinning
   * forward. If they're different, it's going backward.
   */
  lcd.setCursor(8,inputLineSelect);
  // encoder is moving clockwise
  if (digitalRead(encoder0PinA) != digitalRead(encoder0PinB)) {                                    
    if ((inputLineSelect == 0) && ((activeMode == 0) || (activeMode == 1) || (activeMode == 2))){  // input line is 0 and a bulb mode is selected
      if (bulbTime > 240){ //increase increment
        bulbTime += 10;
      }
      else {
        bulbTime++; // increase by 1
      }
      lcd.print(convertTime(bulbTime));  
    }

    else if ((inputLineSelect == 1) && ((activeMode == 1) || (activeMode == 3))){
      if (timelapseTime > 350){  // increase increment when the value is higher
        timelapseTime += 10;
      }
      else {
        timelapseTime++;
      }
      lcd.print(convertTime(timelapseTime));
    }
    else if ((inputLineSelect == 1) && ((activeMode == 2) || (activeMode == 4))){
      if (levelSetting < 1024){  // don't increase past 1024
         levelSetting += 5;
         triggerSensDisplay();
      }
    }
  } 
  // encoder is moving counter clockwise
  else if (digitalRead(encoder0PinA) == digitalRead(encoder0PinB)) {                                     
    if ((inputLineSelect == 0) && ((activeMode == 0) || (activeMode == 1) || (activeMode == 2)) && (bulbTime > 0)){ // input line is 0 and a bulb mode is selected
      if (bulbTime > 250){ // increase decrement when the value is higher
        bulbTime -= 10;
      }
      else {
        bulbTime--; // decrement by 1
      }
      lcd.print(convertTime(bulbTime));
    }
    else if ((inputLineSelect == 1) && ((activeMode == 1) || (activeMode == 3)) && (timelapseTime > 0)){
      if (timelapseTime > 360){
        timelapseTime -= 10;
      }
      else {
        timelapseTime--;
      }
      lcd.print(convertTime(timelapseTime));
    }
    else if ((inputLineSelect == 1) && ((activeMode == 2) || (activeMode == 4))){
      if (levelSetting > 0){
         levelSetting -= 5;
         triggerSensDisplay();      // since this is a bit more fancy it's done in a seperate function.
      }
    }
  }
  if ((inputLineSelect != 1) && ((activeMode != 2) || (activeMode != 4))){
        lcd.setCursor(7,inputLineSelect);
  }
}

