/*
Camera intervalometer and Time Lapse controller
 
 The circuit:
 LCD pins
 * LCD (4) RS pin  to D13
 * LCD (6) Enable pin D12
 * LCD (11) D4 pin to D11
 * LCD (12) D5 pin to D10
 * LCD (13) D6 pin to D9
 * LCD (14) D7 pin to D8
 * LCD (5) R/W pin to ground
 * 10K pot:
 *   ends to +5V and ground
 *   wiper to LCD (3) VO pin (pin 3) for contrast
 *   Vss (1) to ground
 *   Vdd (2) to +5v
 
 Relay pins
 * focus trigger on pin D3 
 * Shutter trigger on pin D7
 
 Button and encoder pins
 * Mode selection button to A0
 * Input selection button to A1
 * start button pin A2
 * encoder pins D2 & D4
 * indicator light D5 & D6 PWM
 
 All sauce by Ryan Cole - 2011 & 2013
 Open source and all that innit.
 */


// PIN VARIABLES
const int runSwitch = A0;     // pin for run switch
const int inputSelector = A1; // pin for input_selector button
const int modeSelector = A2;  // pin for modeSelector button

const int encoder0PinA = 2;   // encoder A pin
const int encoder0PinB = 4;   // encoder B pin

const int ledRed = 5;         // pins for indicator LED
const int ledGreen = 6;       

const int focusTrig = 3;
const int shutterTrig = 7;

const int triggerInput = A3;  // pin for trigger sensor in


// LOGIC VARIABLES

int bulbTime = 0;            // this is the value to be incremented for bulb timer
int timelapseTime = 0;       // this is the value to be incremented for time lapse timer

const int numRows = 2;       // number of rows and cols in LCD
const int numCols = 16;

int activeMode = 0;          // stores the operation mode. Should loop from 0-4. See the 'switch' section below for what each number is.
int inputLineSelect = 0;     // switches between 1 and 0 every time the inputSelector button is pressed, and selects the LCD row.
int playPause = 0;           // button state for run button. 0 when paused, 1 when running
int shotCount = 0;

int inputBtnPressed = 1;     // inputBtnPressed = 0 when the button is up, and 1 when the button is pressed.
int modeBtnPressed = 1;      // modeBtnPressed = 0 when the button is not pressed, and 1 when the button is pressed
int runBtnPressed = 0;       // button state for play btn.

long lastTick = 0;           // stores a time from millis() to denote passage of time. See timer functions.
int timelapseTimeI;          // stores original value of TL to reset after countdown.
int bulbTimeI;               // does the same for BT
int levelSetting = 0;        // trigger threshold set by user
int sensorLevel = 0;         // the actual value read off the sensor pin

char* hh;                    // variables for timer function
char* mm;
char* ss;
int hhI;
int mmI;
int ssI;
static char buffer[9];           // for time count conversion. holds 9 digits to account for 00:00:00

static char trigSetBuffer[5];    // holds converted 4 digit display of levelSetting
static char trigInputbuffer[5];  // holds converted 4 digit display of sensorLevel
int glyphSet;                    // holds bar graph glyph for visual feedback on levelSetting
int glyphIn;                     // holds bar graph glyph for visual feedback on sensorLevel. I added this as a visual indicator of input levels.

byte smiley[8] = {               // byte data for custom glyphs in bar graph. one for each of 7 levels
  B00000,
  B10001,
  B00000,
  B00000,
  B10001,
  B01110,
  B00000,
};

byte trig[8] = {
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
};

byte none[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
};

byte one[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B11111,
};

byte two[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00000,
  B11111,
  B11111,
};

byte three[8] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B11111,
  B11111,
  B11111,
};

byte four[8] = {
  B00000,
  B00000,
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
};

byte five[8] = {
  B00000,
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
};
byte six[8] = {
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
};
byte seven[8] = {
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
};

#include <stdio.h>                        // include the string voodoo library for converting timers to hh:mm:ss
#include <LiquidCrystal.h>                // include the lcd library and initialize which pins are in use
LiquidCrystal lcd(13, 12, 11, 10, 9, 8);  


void setup() {
  //Serial.begin(9600);                     // Initialize the Serial port at 9600 baud. For debugging only
  pinMode(inputSelector,INPUT);           // inputLineSelect button input
  digitalWrite(inputSelector, HIGH);      // turn on pullup resistor
  pinMode(modeSelector, INPUT);           // mode selector pin setup
  digitalWrite(modeSelector, HIGH);       // turn on pullup resistor
  pinMode(runSwitch, INPUT);              // mode selector pin setup
  digitalWrite(runSwitch, HIGH);          // turn on pullup resistor

  pinMode(encoder0PinA, INPUT);           // encoder pin A setup
  digitalWrite(encoder0PinA, HIGH);       // turn on pullup resistor
  pinMode(encoder0PinB, INPUT);           // encoder pin b setup
  digitalWrite(encoder0PinB, HIGH);       // turn on pullup resistor

  pinMode(ledRed, OUTPUT);                // red led
  pinMode(ledGreen, OUTPUT);              // green led

  pinMode(triggerInput, INPUT);           // trigger input
  
  pinMode(focusTrig, OUTPUT);                // pins for camera control
  pinMode(shutterTrig, OUTPUT);

  analogWrite(ledRed, 255);               // turn on red LED for power!
  lcd.createChar(0, smiley);              // converts byte data into usable glyphs. 8 max, so smiley gets killed for 'none' later on.
  lcd.createChar(1, one);
  lcd.createChar(2, two);
  lcd.createChar(3, three);
  lcd.createChar(4, four);
  lcd.createChar(5, five);
  lcd.createChar(6, six);
  lcd.createChar(7, seven);
  lcd.begin(numCols,numRows);             // set up the LCD's number of columns and rows:
  showGreeting();                         // greeting screen on start up. Comment this out if it's annoying.
  //showTimers(activeMode);               // uncomment this if you remove showGreeting(); as no display init will be called otherwise
}

void loop() {
  playButton();                                                        // make the LED red when not running.
  if (playPause == 0){
    analogWrite(ledRed, 255);
    analogWrite(ledGreen, 0);
  } 

  switch (playPause) {
  case 0:                                                              // Run switch is off!!
    attachInterrupt(0, doEncoder, CHANGE);                             // encoder on interrupt 0  
    inputSelectorButton();                                             // listen to input button
    modeSelectorButton();                                              // listen to mode button 
    resetCounters();                                                   // listen to counter reset
    if (activeMode == 0){inputLineSelect = 0;}                         // make sure that the cursor falls on the right line for the selected mode.
    if (activeMode == 3 || activeMode == 4){inputLineSelect = 1;}
    break;
  case 1:                                  // Run switch is on!
    detachInterrupt(0);                    // disable encoder so we don't change a value in use.
    switch (activeMode) {                  // detect what mode is operating and chose the right operations
    case 0:                                // run bulb mode
      bulbMode();
      break;
    case 1:                                // run bulb + timelapse mode
      bulbTimeLapseMode();
      break;
    case 2:                                // run bulb + trigger mode
      bulbTrigMode();
      break;
    case 3:                                // run time lapse mode
      timeLapseMode();
      break;
    case 4:                                // run trigger mode
      trigMode();
      break;
    }
  }
}

