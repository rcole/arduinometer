// Not exactly all time related, but most of the functional guts are in here.

char* convertTime(int time) {
  hhI=time/3600;
  mmI=(time-hhI*3600)/60;
  ssI=time-(hhI*3600)-(mmI*60);
  sprintf(buffer, "%02d:%02d:%02d", hhI, mmI, ssI);
  return buffer;
}

char* fourDigits(int level){
  sprintf(trigSetBuffer, "%04d", level);
  return trigSetBuffer;
}

void bulbCounter(){
  if (millis() - lastTick >= 1000) {
    lastTick = millis();
    bulbTime--;
    lcd.setCursor(8,0);
    lcd.print(convertTime(bulbTime)); 
    //Serial.println(bulbTime);
  } 
}

void timelapseCounter(){
  if (millis() - lastTick >= 1000) {
    lastTick = millis();
    timelapseTime--;
    lcd.setCursor(8,1);
    lcd.print(convertTime(timelapseTime)); 
    //Serial.println(timeLapseTime);
  } 
}

void bulbWhile(){
  while(bulbTime > 0){                       // countdown bulb value
    bulbCounter();
    playButton();
    if (playPause == 0){
      break;
    }
  }
}

void timelapseWhile(){
  while(timelapseTime > 0){                  // count down TL value
    timelapseCounter();
    yellow();                                 
    playButton();
    if (playPause == 0){
      break;
    }  
  }
  shotCount++;
}

void triggerWhile(){
  while (sensorLevel <= levelSetting){
    yellow();                                // make LED yellow to indicate TL running. 
    playButton();
    readSensor();
    if (playPause == 0){
      break;
    }
  }
}

void readSensor(){                              // obviously to read the sensor :)
  if (millis() - lastTick >= 50) {
    lastTick = millis();
    sensorLevel = analogRead(triggerInput);
    lcd.setCursor(9,1);
    lcd.print(fourDigits(sensorLevel));
    glyphSet = sensorLevel / 146;
    lcd.write(glyphSet);
    lcd.setCursor(9,1);
  } 
}

void openShutter(){                            // open the camera shutter
  //Serial.println("shutter opened");
  digitalWrite(focusTrig, HIGH); 
  delay(100);                                  // the delay here is meant to allow the camera time to respond. Might need to tweak the value.
  digitalWrite(shutterTrig, HIGH); 
  green();
}

void closeShutter(){
  //Serial.println("shutter closed");          // close the camera shutter
  digitalWrite(focusTrig, LOW);
  digitalWrite(shutterTrig, LOW); 
  red();
}


