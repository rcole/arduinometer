void playButton(){
  if ((digitalRead(runSwitch) == LOW) && (runBtnPressed == 0)) // run selector Button is pressed. 
  {
    runBtnPressed = 1;                           
    //Serial.println("play pressed");
    if (playPause == 1){
      playPause = 0;
      closeShutter();
    }
    else {
      playPause=1;
    }
  }
  else if ((digitalRead(runSwitch) == HIGH) && (runBtnPressed == 1)) // run Button is released. 
  {
    //Serial.println("play released");
    runBtnPressed = 0;                     // Avoids held press repeating.
    delay(100);                            // button bounce.
  }
}

void inputSelectorButton(){
  if (activeMode == 1 || activeMode == 2){                             // only respond if we're in a mode with 2 values to edit 
    if ((digitalRead(inputSelector) == LOW) && (inputBtnPressed == 0)) // input selector Button is pressed, but was previously not 
    {                                                                  // This switches the line the cursor is set on
      inputBtnPressed = 1;     
      if (inputLineSelect==1){
        inputLineSelect=0;
        lcd.setCursor(7,0);
      }
      else {
        inputLineSelect=1;
        lcd.setCursor(7,1);
      }
    }
    else if ((digitalRead(inputSelector) == HIGH) && (inputBtnPressed == 1)) // input selector Button is released, but was previously was pressed
    {
      inputBtnPressed = 0;                                                   // Avoids held press repeating.
      delay(100);                                                            // small delay to account for button bounce.
    }
  }
}

void modeSelectorButton(){
  if ((digitalRead(modeSelector) == LOW) && (modeBtnPressed == 0)) // mode selector Button is pressed. 
  {
    modeBtnPressed = 1;                // mode selector Button press is registered
    if (activeMode < 4){               // Here is control logic for incrementing the mode variable
      activeMode++;
    }
    else{
      activeMode = 0;
    }
    showMode(activeMode);
  }
  else if ((digitalRead(modeSelector) == HIGH) && (modeBtnPressed == 1)) // mode selector Button is released. 
  {
    modeBtnPressed = 0;                 // Avoids held press repeating.
    delay(300);                         // small delay to account for button bounce.
    showTimers(activeMode);
  }
}

void resetCounters(){
  if ((digitalRead(modeSelector) == LOW) && (digitalRead(runSwitch) == LOW)) // mode selector Button is pressed.
  { 
    lcd.clear();
    lcd.print("reset counters");
    showTimers(activeMode);
  }
  shotCount = 0;
}



