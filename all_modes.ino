void bulbMode(){
  bulbTimeI = bulbTime;                      // store orignal value for future use
  openShutter();                             // open camera shutter
  bulbWhile();                               // count off bulb time
  closeShutter();                            // close camera shutter
  bulbTime = bulbTimeI;                      // reset BT with original value incase you want to run the same exposure again. 
  lcd.setCursor(8,0);                        
  lcd.print(convertTime(bulbTime));          // print it to the display
  playPause = 0;                             // end run
}                                            // this only runs once

void bulbTimeLapseMode(){
  timelapseTimeI = timelapseTime;
  bulbTimeI = bulbTime;
  timelapseWhile();                          // count down TL time and progress when 0
  timelapseTime = timelapseTimeI;            // reset TL to original time value
  lcd.setCursor(8,1);
  lcd.print(convertTime(timelapseTime));     // print it to the display
  if (playPause == 1) openShutter();         // open camera shutter
  bulbWhile();                               // count off bulb time value
  closeShutter();                            // close camera shutter
  bulbTime = bulbTimeI;                      // reset BT with original value
  lcd.setCursor(8,0);
  lcd.print(convertTime(bulbTime));          // print it to the display.
}                                            // rinse, and repeat. This will run till you stop it.

void bulbTrigMode(){
  bulbTimeI = bulbTime;
  triggerWhile();                            // listen to sensor and progress when threshold is met.
  if (playPause == 1) openShutter();         // open camera shutter.
  bulbWhile();                               // count down bulb time and progress when 0
  closeShutter();                            // close camera shutter
  bulbTime = bulbTimeI;                      // reset LCD with original value
  lcd.setCursor(8,0);
  lcd.print(convertTime(bulbTime));          
  playPause = 0;                             // end run after time elapses
}

void timeLapseMode(){
  timelapseTimeI = timelapseTime;            // store orignal value for future use
  timelapseWhile();                          // count down TL time and progress when 0
  timelapseTime = timelapseTimeI;            // reset Tl to original time value
  lcd.setCursor(8,1);
  lcd.print(convertTime(timelapseTime));     // reset LCD with original value
  lcd.setCursor(2,1);
  lcd.print("/   ");
  lcd.setCursor(3,1);
  lcd.print(shotCount);
  if (playPause == 1) openShutter();         // open camera shutter
  delay(200);                                // the delay here is meant to allow the camera time to respond. Might need to tweak the value.
  closeShutter();                            // close camera shutter. The actual exposure time should be set on the camera.
  }                                          // rinse, and repeat. This will run till you stop it.


void trigMode(){
  while (playPause == 1){
  triggerWhile();                            // listen to sensor and progress when threshold is met.             
  if (playPause == 1) openShutter();         // open camera shutter.
  delay(200);                                // the delay here is meant to allow the camera time to respond. Might need to tweak the value.
  closeShutter();                            // close camera shutter. The actual exposure time should be set on the camera.
  playPause = 0;                             // end run.
  }
}

